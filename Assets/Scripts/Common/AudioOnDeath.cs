using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioOnDeath : MonoBehaviour
{
    private AudioSource audioSource;
    private Health health;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        health = GetComponent<Health>();
        health.OnDie += PlayAudio;
        health.WaitBeforeDestroy++;
    }

    private void PlayAudio()
    {
        StartCoroutine(PlayAudioClip());
    }

    private IEnumerator PlayAudioClip()
    {
        audioSource.Play();
        yield return new WaitForSeconds(audioSource.clip.length);

        health.WaitBeforeDestroy--;
    }
}
