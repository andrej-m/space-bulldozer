using UnityEngine;

public enum HardpointType { ForwardShooting, Turret };

[System.Serializable]
public class EquipmentSlot
{
    public Transform Hardpoint;
    public GameObject WeaponPrefab;
    public HardpointType Type;
}

public class WeaponInventory : MonoBehaviour
{
    [Tooltip("Equipped weapons")]
    public EquipmentSlot[] WeaponSlots;

    // Start is called before the first frame update
    private void Start()
    {
        foreach (EquipmentSlot slot in WeaponSlots)
        {
            if (slot.Hardpoint != null && slot.WeaponPrefab != null)
            {
                GameObject instance = Instantiate(slot.WeaponPrefab, slot.Hardpoint.position, slot.Hardpoint.rotation);
                instance.transform.parent = slot.Hardpoint;
            }
        }
    }

    public void Equip(EquipmentSlot weaponSlot, GameObject weaponPrefab)
    {
        // TODO: check if provided slot belongs to object's WeaponsSlot
        // TODO: destroy exsiting weapon
        weaponSlot.WeaponPrefab = weaponPrefab;
        GameObject instance = Instantiate(weaponSlot.WeaponPrefab, weaponSlot.Hardpoint.position, weaponSlot.Hardpoint.rotation);
        instance.transform.parent = weaponSlot.Hardpoint;
    }
}
