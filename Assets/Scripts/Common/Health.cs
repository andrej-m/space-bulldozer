using System.Collections;
using UnityEngine;

public class Health : MonoBehaviour
{
    /* Components should increment this on Start(), attach to OnDie(), handle whatever
     * they need to handle during the death sequence, then decrement it when they are
     * done so that OnFinishDying() can trigger */
    public int WaitBeforeDestroy { get; set; }

    /* Triggered when health reaches zero and object should start the death sequence,
     * i.e. when WaitBeforeDestroy equals zero */
    public System.Action OnDie = delegate { };

    /* Triggered when death sequence finishes */
    public System.Action OnFinishDying = delegate { };

    /* Triggered when amount of health changes */
    public System.Action OnHealthChange = delegate { };

    public bool IsDead { get; private set; } = false;

    /* Amount of shield hitpoints when object is spawned */
    [Min(0f)]
    public int InitialShieldHitpoints;

    /* Maximum amount of shield hitpoints */
    private int maxShields;
    public int MaxShieldHitpoints {
        get { return maxShields; }
        set {
            maxShields = value;
            OnHealthChange();
        }
    }

    /* Current shield hitpoints */
    private int currentShields;
    public int ShieldHitpoints {
        get { return currentShields; }
        set {
            currentShields = value;
            OnHealthChange();
        }
    }

    /* Amount of hull hitpoints when object is spawned */
    [Min(0f)]
    public int InitialHullHitpoints;
    public int MaxHullHitpoints { get; set; }
    public int HullHitpoints { get; set; }

    private Collider2D colliderSelf;

    private void Start()
    {
        MaxShieldHitpoints = InitialShieldHitpoints;
        ShieldHitpoints = InitialShieldHitpoints;

        MaxHullHitpoints = InitialHullHitpoints;
        HullHitpoints = InitialHullHitpoints;

        colliderSelf = GetComponentInChildren<Collider2D>();
        OnHealthChange();
    }

    public void TakeDamage(DealDamage from)
    {
        int shieldHitpointsBefore = ShieldHitpoints;
        int hullHitpointsBefore = HullHitpoints;

        // Take shield damage
        ShieldHitpoints -= from.ShieldDamage;
        if (ShieldHitpoints < 0)
        {
            ShieldHitpoints = 0;
        }

        // Hull takes damage only when shields are gone
        if (ShieldHitpoints == 0)
        {
            HullHitpoints -= from.HullDamage;
            if (HullHitpoints < 0)
            {
                HullHitpoints = 0;
            }
        }

        if (shieldHitpointsBefore != ShieldHitpoints || hullHitpointsBefore != HullHitpoints)
        {
            OnHealthChange();
        }

        // Die when hull hitpoints reach zero
        if (HullHitpoints == 0)
        {
            Die();
        }
    }

    public void Die()
    {
        IsDead = true;

        // Stop further collisions in case more things need to happen before gameobject is
        // destroyed, e.g. death animations finish playing
        if (colliderSelf != null)
        {
            colliderSelf.enabled = false;
        }
        OnDie();
        StartCoroutine(FinishDying());
    }

    private IEnumerator FinishDying()
    {
        yield return new WaitUntil(() => WaitBeforeDestroy == 0);
        
        OnFinishDying();
        
        Destroy(gameObject);
    }
}
