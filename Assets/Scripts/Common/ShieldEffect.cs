using System.Collections;
using UnityEngine;

public class ShieldEffect : MonoBehaviour
{
    private Health health;
    private SpriteRenderer shieldRenderer;
    private Collider2D shieldCollider;
    private float shieldFlickerDuration = 1.5f;
    private int prevShieldHitpoints;

    private void Start()
    {
        health = GetComponentInParent<Health>();
        if (!health)
        {
            Debug.LogError("Could not find Health component in parent: " + GetInstanceID());
        }

        shieldRenderer = GetComponent<SpriteRenderer>();
        shieldCollider = GetComponent<Collider2D>();
        health.OnHealthChange += ShowShields;
        prevShieldHitpoints = health.ShieldHitpoints;
        HideShields();
    }

    private void ShowShields()
    {
        if (prevShieldHitpoints > 0)
        {
            shieldRenderer.enabled = true;
            shieldCollider.enabled = true;
            StartCoroutine(HideShieldsAfterTime());
        } else
        {
            shieldCollider.enabled = false;
        }

        prevShieldHitpoints = health.ShieldHitpoints;
    }

    private void HideShields()
    {
        shieldRenderer.enabled = false;
    }

    IEnumerator HideShieldsAfterTime()
    {
        float time_start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup - time_start < shieldFlickerDuration)
        {
            shieldRenderer.color = new Color(shieldRenderer.color.r, shieldRenderer.color.g, shieldRenderer.color.b, Random.Range(0.4f, 0.6f));
            yield return null;
        }

        HideShields();
    }
}
