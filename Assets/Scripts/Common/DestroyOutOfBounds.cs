using UnityEngine;

public class DestroyOutOfBounds : MonoBehaviour
{
    private float xBound = 50;
    private float yBound = 50;

    void Update()
    {
        if (transform.position.x > xBound || transform.position.x < -xBound || transform.position.y > yBound || transform.position.y < -yBound)
        {
            Destroy(gameObject);
        }
    }
}
