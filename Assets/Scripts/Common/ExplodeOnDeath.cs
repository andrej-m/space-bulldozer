using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class ExplodeOnDeath : MonoBehaviour
{
    public Explosion ExplosionEffect;
    
    private Health health;

    private void Start()
    {
        health = GetComponent<Health>();

        health.OnDie += DeactivateOriginalSprites;
        health.OnDie += SpawnExplosion;
        health.WaitBeforeDestroy++;
    }

    private void DeactivateOriginalSprites()
    {
        foreach (var sprite in GetComponentsInChildren<SpriteRenderer>())
        {
            sprite.enabled = false;
        }
    }

    private void SpawnExplosion()
    {
        if (ExplosionEffect == null)
        {
            Debug.LogWarning("No explosion effect set for '" + name + "'");
            return;
        }

        Explosion instance = Instantiate(ExplosionEffect, transform.position, transform.rotation);
        StartCoroutine(AnimationFinished(instance));
    }

    private IEnumerator AnimationFinished(Explosion explosion)
    {
        yield return new WaitUntil(() => explosion.IsDone == true);

        health.WaitBeforeDestroy--;
    }
}
