using UnityEngine;

public class DealDamage : MonoBehaviour
{
    [Min(0f)]
    public int ShieldDamage = 0;

    [Min(0f)]
    public int HullDamage = 0;

    public System.Action<GameObject> OnEnemyDestroyed = delegate (GameObject enemy) { };

    /* OnCollisionEnter is used to deal meelee damage between both object */
    private void OnCollisionEnter2D(Collision2D collision)
    {
        /* Deal damage to target */
        DoDamage(collision.gameObject.GetComponent<Health>());

        /* Receive damage from target */
        Health thisHealth = GetComponent<Health>();
        DealDamage targetDamage = collision.gameObject.GetComponent<DealDamage>();
        if (targetDamage != null)
        {
            targetDamage.DoDamage(thisHealth);
        }
    }

    /* Apply damage to target */
    public void DoDamage(Health target)
    {
        if (target != null)
        {
            target.TakeDamage(this);
            if (target.IsDead)
            {
                OnEnemyDestroyed(target.gameObject);
            }
        }
    }
}
