using UnityEngine;

public class AStarPeriodicGraphRescan : MonoBehaviour
{
    void Start()
    {
        InvokeRepeating("RescanGraph", 0f, 0.5f);
    }

    void RescanGraph()
    {
        AstarPath.active.Scan();
    }
}
