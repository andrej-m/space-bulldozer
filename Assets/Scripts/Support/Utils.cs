using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Utils
{
    /* Return closest positive or negative angle that can be applied to `self` 
     * transform to face the `target` transform. */
    public static float ClosestAngleToTargetXAxis(Transform self, Transform target)
    {
        // Target's position relative to self
        Vector2 targetLocalPosition = new Vector2(target.position.x - self.position.x, target.position.y - self.position.y);

        // Angle between self's X axis and target position
        float angle = Mathf.Atan2(targetLocalPosition.y, targetLocalPosition.x) * Mathf.Rad2Deg; // Returns -180..180
        
        // Convert negative angles -180..-0 into 180..360
        if (angle < 0f)
        {
            angle = 360f + angle;
        }

        // Angle difference between where self if facing and where target is located
        float currentRotation = self.rotation.eulerAngles.z; // Returns 0..360
        float angleDiff = angle - currentRotation;

        // Take rotation that would make self face the target faster: clockwise or counter clockwise
        if (angleDiff > 180f)
        {
            angleDiff = (360f - angleDiff) * -1;
        }
        else if (angleDiff < -180f)
        {
            angleDiff = (360f + angleDiff);
        }

        return angleDiff;
    }

    /* Return clipped (cut off) value if it's absolute value is larger than allowed
     * maximum. Otherwise return value itself. */
    public static float ClipValue(float value, float maximumAllowed)
    {
        maximumAllowed = Mathf.Abs(maximumAllowed);

        if (value < 0f)
        {
            return Mathf.Max(value, -maximumAllowed);
        }
        else
        {
            return Mathf.Min(value, maximumAllowed);
        }
    }


    /* Return list of prefabs of type T. Unity Editor only method, returns empty list in build. */
    public static List<T> FindPrefabsByType<T>() where T : MonoBehaviour
    {
        List<T> result = new List<T>();
#if UNITY_EDITOR
        foreach (var prefab in Resources.FindObjectsOfTypeAll<T>())
        {
            if (EditorUtility.IsPersistent(prefab) == true)
            {
                result.Add(prefab);
            }
        }
#endif
        return result;
    }
}
