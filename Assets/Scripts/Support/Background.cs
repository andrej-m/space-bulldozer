using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    Renderer rendererSelf;

    float scrollSpeed = -0.2f;

    void Awake()
    {
        rendererSelf = GetComponent<Renderer>();
    }

    private void Update()
    {
        Vector2 textureOffset = rendererSelf.material.GetTextureOffset("_MainTex");
        float scrollDistance = Time.deltaTime * scrollSpeed;
        float xOffset = textureOffset.x + scrollDistance;

        Vector2 newTextureOffset = new Vector2(xOffset, textureOffset.y);
        rendererSelf.material.SetTextureOffset("_MainTex", newTextureOffset);
    }
}
