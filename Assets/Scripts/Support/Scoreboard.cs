using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scoreboard : MonoBehaviour
{
    public PlayerShip player;
    public TextMeshProUGUI scoreText;

    private Dictionary<string, int> scoreboard = new Dictionary<string, int>();
    public int totalScore = 0;

    private void Start()
    {
        player.OnEnemyDestroyed += EnemyDestroyed;
        UpdateScoreboardText();
    }

    private void EnemyDestroyed(GameObject enemy)
    {
        ScoreValue score = enemy.GetComponent<ScoreValue>();
        if (score == null)
        {
            return;
        }

        if (scoreboard.ContainsKey(score.ScoreName) == false)
        {
            scoreboard[score.ScoreName] = 0;
        }

        scoreboard[score.ScoreName] += score.Value;
        totalScore += score.Value;

        UpdateScoreboardText();
    }

    void UpdateScoreboardText()
    {
        if (scoreText == null)
        {
            return;
        }

        string s = "Total score: " + totalScore;
        foreach (KeyValuePair<string, int> entry in scoreboard)
        {
            s += "\n" + entry.Key + ": " + entry.Value;
        }
        scoreText.text = s;
    }
}
