using UnityEngine;

public class Explosion : MonoBehaviour
{
    public bool IsDone = false;
    /* Called by animation event */
    private void AnimationFinished()
    {
        IsDone = true;
        Destroy(gameObject);
    }
}
