using UnityEngine;

public interface IPowerup
{
    bool ApplyPowerupToTarget(GameObject target);
}
