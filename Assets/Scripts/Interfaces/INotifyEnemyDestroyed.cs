using UnityEngine;

public interface INotifyEnemyDestroyed
{
    void NotifyEnemyDestroyed(GameObject enemy);
}
