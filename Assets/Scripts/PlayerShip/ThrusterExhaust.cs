using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Animator))]
public class ThrusterExhaust : MonoBehaviour
{
    public enum MoveDirection { Left, Right, Up, Down };
    public MoveDirection PlayerDirection;

    private PlayerInput input;
    private Animator animator;

    private void Start()
    {
        input = GetComponentInParent<PlayerInput>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        FireThruster();
    }

    private void FireThruster()
    {
        animator.SetBool("FireThruster", ShouldFireThruster());
    }

    /* Determine if thruster should be fired depending on Direction setting and 
     * current player's input */
    private bool ShouldFireThruster()
    {
        bool shouldFire = false;

        switch (PlayerDirection)
        {
            case MoveDirection.Left:
                if (input.Horizontal < 0f)
                {
                    shouldFire = true;
                }
                break;

            case MoveDirection.Right:
                if (input.Horizontal > 0f)
                {
                    shouldFire = true;
                }
                break;

            case MoveDirection.Up:
                if (input.Vertical > 0f)
                {
                    shouldFire = true;
                }
                break;

            case MoveDirection.Down:
                if (input.Vertical < 0f)
                {
                    shouldFire = true;
                }
                break;

            default:
                Debug.LogError("This is unexpected: unhandled direction '" + PlayerDirection + "'");
                break;
        }

        return shouldFire;
    }
}
