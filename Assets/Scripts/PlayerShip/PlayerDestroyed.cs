using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDestroyed : MonoBehaviour
{
    Health health;

    // Start is called before the first frame update
    void Awake()
    {
        health = GetComponentInChildren<Health>();
        health.OnFinishDying += ExitToMainMenu;
    }

    private void ExitToMainMenu()
    {
        // Go to main menu scene
        SceneManager.LoadScene(0);
    }
}
