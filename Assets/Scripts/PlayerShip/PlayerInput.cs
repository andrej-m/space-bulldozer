using System;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class PlayerInput : MonoBehaviour
{
    public float Horizontal { get; private set; }
    public float Vertical { get; private set; }

    public event Action OnFire = delegate { };

    private Health health;

    private void Start()
    {
        health = GetComponent<Health>();
    }

    void Update()
    {
        if (health.IsDead)
        {
            Horizontal = 0f;
            Vertical = 0f;
            return;
        }

        Horizontal = Input.GetAxis("Horizontal");
        Vertical = Input.GetAxis("Vertical");
        if (Input.GetButton("Fire1"))
        {
            OnFire();
        }
    }
}
