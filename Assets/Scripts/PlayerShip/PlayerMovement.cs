using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private PlayerInput playerInput;
    private ShipMovementSpeed movementSpeed;
    
    private (float min, float max) positionConstraintsXRange = (-28f, 28f);
    private (float min, float max) positionConstraintsYRange = (-15f, 15f);

    void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        movementSpeed = GetComponentInChildren<ShipMovementSpeed>();
    }

    void Update()
    {
        // Calculate new position based on player's move input
        Vector3 positionDiff = new Vector3(playerInput.Horizontal * Time.deltaTime * movementSpeed.MovementSpeed, playerInput.Vertical * Time.deltaTime * movementSpeed.MovementSpeed, 0);
        Vector3 newPosition = transform.position + positionDiff;

        // Constrain position within X and Y range
        newPosition.x = Mathf.Max(newPosition.x, positionConstraintsXRange.min);
        newPosition.x = Mathf.Min(newPosition.x, positionConstraintsXRange.max);
        newPosition.y = Mathf.Max(newPosition.y, positionConstraintsYRange.min);
        newPosition.y = Mathf.Min(newPosition.y, positionConstraintsYRange.max);

        transform.position = newPosition;
    }
}
