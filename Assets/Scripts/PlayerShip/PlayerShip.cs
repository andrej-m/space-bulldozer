using UnityEngine;

public class PlayerShip : MonoBehaviour, INotifyEnemyDestroyed, ITakesPowerups
{
    public System.Action<GameObject> OnEnemyDestroyed = delegate { };

    public void NotifyEnemyDestroyed(GameObject enemy)
    {
        OnEnemyDestroyed(enemy);
    }
}
