using UnityEngine;

public class RestRotation : MonoBehaviour
{
    [Min(0f)]
    public float RotationSpeedRange = 0f;

    private float RotationSpeed;
    private SpriteRenderer sprite;

    private void Awake()
    {
        RotationSpeed = Random.Range(-RotationSpeedRange, RotationSpeedRange);
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    void Update()
    {
        if (RotationSpeed != 0f)
        {
            sprite.transform.Rotate(Vector3.forward, RotationSpeed * Time.deltaTime);
        }
    }
}
