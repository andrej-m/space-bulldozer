using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public List<Obstacle> obstaclePrefabs;

    private float xSpawn = 30;
    private float ySpawnRange = 13;
    private float startDelay = 2f;
    private float spawnInterval = 5f;

    private void Awake()
    {
        InvokeRepeating("SpawnObstacle", startDelay, spawnInterval);
    }

    private void SpawnObstacle()
    {
        // Randomize spawn position
        Vector2 spawnPosition = new Vector2(xSpawn, Random.Range(-ySpawnRange, ySpawnRange));

        // Randomize type of obstacle to spawn
        int obstacleIndex = Random.Range(0, obstaclePrefabs.Count);

        // Instantiate obstacle at position, with rotation.. TODO
        Instantiate(obstaclePrefabs[obstacleIndex], spawnPosition, obstaclePrefabs[obstacleIndex].transform.rotation);
    }

    [ContextMenu("Autofill obstacles")]
    private void AutoFillPrefabs()
    {
        obstaclePrefabs = Utils.FindPrefabsByType<Obstacle>();
    }
}
