using UnityEngine;

public class MoveForward : MonoBehaviour
{
    public float MoveForwardSpeed = 5f;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * Time.deltaTime * MoveForwardSpeed, Space.World);
    }
}
