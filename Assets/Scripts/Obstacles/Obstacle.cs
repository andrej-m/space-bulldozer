using UnityEngine;

/* This component is intended to be included by all obstacles, so that they
 * can all be grouped together, e.g. using FindObjectsOfType<Obstacle>(). */
public class Obstacle : MonoBehaviour
{
}
