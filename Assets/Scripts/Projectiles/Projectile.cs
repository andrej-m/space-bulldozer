using UnityEngine;

[RequireComponent(typeof(ProjectileFlight))]
[RequireComponent(typeof(DestroyOutOfBounds))]
[RequireComponent(typeof(DealDamage))]
[RequireComponent(typeof(Health))]
public class Projectile : MonoBehaviour
{
    /* Shooter that fired the projectile */
    public GameObject shooter;
    private DealDamage damage;
    private Health health;

    private INotifyEnemyDestroyed shooterToNotify;

    private void Start()
    {
        damage = GetComponent<DealDamage>();
        damage.OnEnemyDestroyed += NotifyShooterOnEnemyDestroyed;

        if (shooter != null)
        {
            shooterToNotify = shooter.GetComponent<INotifyEnemyDestroyed>();
        }

        health = GetComponent<Health>();
    }

    private void NotifyShooterOnEnemyDestroyed(GameObject enemy)
    {
        if (shooterToNotify != null)
        {
            shooterToNotify.NotifyEnemyDestroyed(enemy);
        }
    }

    /* Triggers are used to deal damage by projectiles */
    private void OnTriggerEnter2D(Collider2D collision)
    {
        damage.DoDamage(collision.gameObject.GetComponentInParent<Health>());

        /* Start death sequence for projectile */
        health.Die();
    }
}
