using UnityEngine;

public class AnimateProjectileHit : MonoBehaviour
{
    private Animator animator;

    Health health;

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();

        health = GetComponent<Health>();
        health.OnDie += Hit;
        health.WaitBeforeDestroy++;
    }

    private void Hit()
    {
        if (animator != null)
        {
            animator.SetTrigger("Hit");
        }
    }

    private void HitAnimationFinished()
    {
        health.WaitBeforeDestroy--;
    }
}
