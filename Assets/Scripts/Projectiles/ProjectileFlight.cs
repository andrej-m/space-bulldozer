using UnityEngine;

public class ProjectileFlight : MonoBehaviour
{
    [Min(0)]
    public float FlightSpeed = 1f;
    
    public void Update()
    {
        transform.Translate(Vector3.right * FlightSpeed * Time.deltaTime);
    }
}
