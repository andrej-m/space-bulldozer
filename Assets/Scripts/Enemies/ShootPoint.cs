using System.Collections.Generic;
using UnityEngine;

public class ShootPoint : MonoBehaviour
{
    public Projectile projectile;
    public float reloadTime = 1f;

    private List<Collider2D> parentColliders = new List<Collider2D>();

    float lastFireTime = 0f;

    private void Start()
    {
        foreach (var collider in transform.parent.parent.gameObject.GetComponentsInChildren<Collider2D>())
        {
            parentColliders.Add(collider);
        }
    }

    public void Fire()
    {
        if (CanFire())
        {
            Projectile instance = Instantiate(projectile, transform.position, transform.rotation);

            // Ignore collisions with ship that spawned the projectile
            foreach (var collider in parentColliders)
            {
                Physics2D.IgnoreCollision(instance.GetComponent<Collider2D>(), collider);
            }

            lastFireTime = Time.realtimeSinceStartup;
        }
    }

    public bool CanFire()
    {
        return (Time.realtimeSinceStartup - lastFireTime) > reloadTime;
    }
}
