using UnityEngine;
using Pathfinding;
using System.Collections.Generic;

public class BasicShooter : MonoBehaviour
{
    public Transform target;

    [Min(0.01f)]
    public float rotationSpeed = 1f;

    [Min(0.00f)]
    public float shootAngle = 25f;

    [Min(0.00f)]
    public float speed = 100f;

    [Min(0.00f)]
    public float nextWaypointDistance = 2f;

    Path path;
    int currentWaypoint = 0;
    ShootPoint[] shootPoints;

    GameObject sprite;
    Seeker seeker;
    Rigidbody2D rigidBody;
    Collider2D selfCollider;
    List<Collider2D> targetColliders = new List<Collider2D>();

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponentInChildren<SpriteRenderer>().gameObject;
        seeker = GetComponent<Seeker>();
        rigidBody = GetComponent<Rigidbody2D>();
        shootPoints = GetComponentsInChildren<ShootPoint>();
        selfCollider = GetComponentInChildren<Collider2D>();
        foreach (var collider in target.GetComponentsInChildren<Collider2D>())
        {
            targetColliders.Add(collider);
        }

        InvokeRepeating("UpdatePath", 0f, 0.5f);
    }

    void FixedUpdate()
    {
        RotateTowardsTarget();
        MoveTowardsPathWaypoint();
        FacePlayerLeftOrRight();
        FireIfClearSightToTarget();
    }

    void UpdatePath()
    {
        if (seeker.IsDone())
        {
            // Generate path from current position to target's position
            seeker.StartPath(rigidBody.position, target.position, OnPathComplete);
        }
    }

    void OnPathComplete(Path path)
    {
        // Return if path couldn't be generated
        if (path.error == true)
        {
            Debug.LogError("Failed to generate A* path for " + gameObject.name + "; reason: " + path.errorLog);
            return;
        }

        // Set new path
        this.path = path;
        currentWaypoint = 0;
    }

    /* Rotate towards the direction of the target, within bounds set by `rotationSpeed`. */
    void RotateTowardsTarget()
    {
        float angleTowardsTarget = Utils.ClosestAngleToTargetXAxis(sprite.transform, target);
        float angleToApply = Utils.ClipValue(angleTowardsTarget, rotationSpeed);
        float currentAngle = sprite.transform.rotation.eulerAngles.z;
        sprite.transform.rotation = Quaternion.Euler(0, 0, currentAngle + angleToApply);
    }

    /* Move object towards the path waypoint */
    void MoveTowardsPathWaypoint()
    {
        if (path == null)
        {
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count)
        {
            return;
        }

        // Move towards current path waypoint
        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rigidBody.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;
        rigidBody.AddForce(force);

        // Go to next waypoint if current waypoint is within set distance
        float distance = Vector2.Distance(rigidBody.position, path.vectorPath[currentWaypoint]);
        if (distance < nextWaypointDistance)
        {
            currentWaypoint++;
        }        
    }

    /* Mirror transform right to left (across vertical axis) when shooter is
     * facing towards left side. Assumes that the shooter's normal (reset)
     * position is facing to the right side. */
    void FacePlayerLeftOrRight()
    {
 
        float currentAngle = sprite.transform.rotation.eulerAngles.z;
        if (currentAngle > 270f || currentAngle < 90f)
        {
            // Facing left to right
            sprite.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            // Facing right to left
            sprite.transform.localScale = new Vector3(1f, -1f, 1f);
        }
    }

    /* True if target is within the firing angle set by `shootAngle`. */
    bool TargetWithinFiringAngle()
    {
        float angleTowardsTarget = Utils.ClosestAngleToTargetXAxis(sprite.transform, target);
        return (Mathf.Abs(angleTowardsTarget) <= shootAngle);
    }

    /* Return true if sight is clear of non-target colliders. */
    bool SightClearOfNonTargets()
    {
        ContactFilter2D contactFilter = new ContactFilter2D();
        contactFilter.layerMask = ~(LayerMask.GetMask("Projectiles") + LayerMask.GetMask("Shields")); // Ignore projectiles and shields
        contactFilter.useLayerMask = true;

        RaycastHit2D[] results = new RaycastHit2D[2]; // First element for self collider, second element for target/non-target collider
        int numRaycastHits = Physics2D.Raycast(sprite.transform.position, sprite.transform.TransformDirection(Vector2.right), contactFilter, results);
        bool targetInSight = false;
        for (int i = 0; i < numRaycastHits && i < results.Length; i++)
        {
            Collider2D other = results[i].collider;

            /* The first collider RayCast finds belongs to current object (if it has one
             * attached) and should be ignored */
            if (other.GetInstanceID() == selfCollider.GetInstanceID())
            {
                continue;
            }

            foreach (var targetCollider in targetColliders)
            {
                if (other.GetInstanceID() == targetCollider.GetInstanceID())
                {
                    targetInSight = true;
                    break;
                }
            }
        }

        return targetInSight;
    }

    void FireIfClearSightToTarget()
    {
        if (TargetWithinFiringAngle() == false)
        {
            return;
        }

        if (SightClearOfNonTargets() == false)
        {
            return;
        }

        // Fire anything available
        foreach (ShootPoint shootPoint in shootPoints)
        {
            shootPoint.Fire();
        }
    }
}
