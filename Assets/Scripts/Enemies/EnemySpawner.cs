using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public List<Enemy> enemyPrefabs;
    public Transform target;

    private float xSpawn = 30;
    private float ySpawnRange = 13;
    private float startDelay = 1f;
    private float spawnInterval = 4f;

    private void Awake()
    {
        InvokeRepeating("SpawnEnemy", startDelay, spawnInterval);
    }

    private void SpawnEnemy()
    {
        // Randomize spawn position
        Vector2 spawnPosition = new Vector2(xSpawn, Random.Range(-ySpawnRange, ySpawnRange));

        // Randomize type of enemy to spawn
        int enemyIndex = Random.Range(0, enemyPrefabs.Count);

        // Instantiate enemies at position
        GameObject enemy = Instantiate(enemyPrefabs[enemyIndex].gameObject, spawnPosition, enemyPrefabs[enemyIndex].transform.rotation);
        enemy.GetComponent<BasicShooter>().target = target;
    }

    [ContextMenu("Autofill enemies")]
    private void AutoFillPrefabs()
    {
        enemyPrefabs = Utils.FindPrefabsByType<Enemy>();
    }
}
