using UnityEngine;

public class TrackMousePosition : MonoBehaviour
{
    void Update()
    {
        // Convert mouse position from pixel coordinates to world coordinates
        Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Local mouse position, i.e. relative to the current transform
        Vector2 mouseLocalPosition = new Vector2(mouseWorldPosition.x - transform.position.x, mouseWorldPosition.y - transform.position.y);

        // Angle between transform's X axis and mouse position
        float angle = Mathf.Atan2(mouseLocalPosition.y, mouseLocalPosition.x) * Mathf.Rad2Deg;

        // Set rotation in the direction of mouse position
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }
}
