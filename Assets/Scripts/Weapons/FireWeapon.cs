using System.Collections.Generic;
using UnityEngine;

public class FireWeapon : MonoBehaviour
{
    public Projectile projectile;
    public AudioSource audioSource;

    private List<Collider2D> parentColliders = new List<Collider2D>();

    [Min(0f)]
    public float reloadTime = 1f;

    private float lastFireTime = 0f;

    private void Start()
    {
        /* Attach to player controls, if available */
        PlayerInput player = GetComponentInParent<PlayerInput>();
        if (player != null)
        {
            player.OnFire += Fire;
        }

        foreach (var collider in transform.parent.parent.gameObject.GetComponentsInChildren<Collider2D>())
        {
            parentColliders.Add(collider);
        }
    }
    
    private void Fire()
    {
        if (CanFire() == false)
        {
            return;
        }

        Projectile instance = Instantiate(projectile, transform.position, transform.rotation);
        instance.shooter = GetComponentInParent<PlayerShip>().gameObject;

        // Ignore collisions with ship colliders that spawned the projectile
        foreach (var collider in parentColliders)
        {
            Physics2D.IgnoreCollision(instance.GetComponent<Collider2D>(), collider);
        }

        lastFireTime = Time.realtimeSinceStartup;

        if (audioSource != null)
        {
            audioSource.Play();
        }
    }

    public bool CanFire()
    {
        return (Time.realtimeSinceStartup - lastFireTime) > reloadTime;
    }
}
