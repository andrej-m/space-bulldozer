using System.Collections.Generic;
using UnityEngine;

public class PowerupSpawner : MonoBehaviour
{
    public List<Powerup> powerupPrefabs;

    private float xSpawn = 30;
    private float ySpawnRange = 13;
    private float startDelay = 2f;
    private float spawnInterval = 5f;

    private void Awake()
    {
        InvokeRepeating("SpawnPowerup", startDelay, spawnInterval);
    }

    private void SpawnPowerup()
    {
        // Randomize spawn position
        Vector2 spawnPosition = new Vector2(xSpawn, Random.Range(-ySpawnRange, ySpawnRange));

        // Randomize type of powerup to spawn
        int powerupIndex = Random.Range(0, powerupPrefabs.Count);

        // Instantiate powerup at position
        Instantiate(powerupPrefabs[powerupIndex], spawnPosition, powerupPrefabs[powerupIndex].transform.rotation);
    }

    [ContextMenu("Autofill powerups")]
    private void AutoFillPrefabs()
    {
        powerupPrefabs = Utils.FindPrefabsByType<Powerup>();
    }
}
