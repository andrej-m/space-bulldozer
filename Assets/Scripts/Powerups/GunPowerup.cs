using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Powerup))]
public class GunPowerup : MonoBehaviour, IPowerup
{
    public GameObject WeaponPrefab;
    private HardpointType WeaponHardpointType;

    public bool ApplyPowerupToTarget(GameObject target)
    {
        WeaponInventory inventory = target.GetComponentInParent<WeaponInventory>();
        if (inventory == null)
        {
            return false;
        }

        List<EquipmentSlot> suitableSlots = new List<EquipmentSlot>();
        foreach (EquipmentSlot slot in inventory.WeaponSlots)
        {
            /* Skip if inventory alrady contains this weapon type */
            if (slot.WeaponPrefab != null && slot.WeaponPrefab.name == WeaponPrefab.name)
            {
                return false;
            }

            if (slot.Type == WeaponHardpointType)
            {
                suitableSlots.Add(slot);
            }
        }
        
        /* Auto equip if a free suitable slot exists */
        foreach (EquipmentSlot slot in inventory.WeaponSlots)
        {
            if (slot.WeaponPrefab == null)
            {
                //slot.WeaponPrefab = WeaponPrefab;
                inventory.Equip(slot, WeaponPrefab);
                break;
            }
        }

        // TODO: dialog if all slots are full

        return true;
    }
}
