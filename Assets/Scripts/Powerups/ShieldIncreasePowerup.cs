using UnityEngine;

public class ShieldIncreasePowerup : MonoBehaviour, IPowerup
{
    private int shieldHitpointBonus = 115;

    public bool ApplyPowerupToTarget(GameObject target)
    {
        Health health = target.GetComponentInParent<Health>();
        if (health == null)
        {
            return false;
        }

        health.MaxShieldHitpoints += shieldHitpointBonus;

        return true;
    }
}
