using UnityEngine;

/* This component is intended to be included by all powerups, so that they
 * can all be grouped together, e.g. using FindObjectsOfTypeAll<Obstacle>(). */
public class Powerup : MonoBehaviour
{
}
