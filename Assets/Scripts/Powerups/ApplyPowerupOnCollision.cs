using UnityEngine;

public class ApplyPowerupOnCollision : MonoBehaviour
{
    Health health;
    IPowerup powerup;

    private void Start()
    {
        health = GetComponent<Health>();
        if (health == null)
        {
            Debug.LogError("Expected Health component on '" + gameObject.name + "'");
        }

        powerup = GetComponent<IPowerup>();
        if (powerup == null)
        {
            Debug.LogError("Expected IPowerup interface on '" + gameObject.name + "'");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /* Apply powerup to target and destroy self it it was successful */
        if (powerup.ApplyPowerupToTarget(collision.gameObject) == true)
        {
            health.Die();
        }
    }
}
