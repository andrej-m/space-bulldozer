using UnityEngine;

public class ShieldUpPowerup : MonoBehaviour, IPowerup
{
    private int shieldHitpointsRestored = 50;
    public bool ApplyPowerupToTarget(GameObject target)
    {
        Health health = target.GetComponentInParent<Health>();
        if (health == null)
        {
            return false;
        }

        if (health.ShieldHitpoints >= health.MaxShieldHitpoints)
        {
            return false;
        }

        health.ShieldHitpoints = Mathf.Min(health.ShieldHitpoints + shieldHitpointsRestored, health.MaxShieldHitpoints);

        return true;
    }
}
