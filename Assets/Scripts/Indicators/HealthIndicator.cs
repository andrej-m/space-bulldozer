using UnityEngine;
using TMPro;

public class HealthIndicator : MonoBehaviour
{
    public Health target;
    private TextMeshProUGUI healthText;

    private void Awake()
    {
        if (target == null)
        {
            Debug.LogWarning("HealthIndicator shouldn't be placed on an object without 'Health' component: " + gameObject.name + " (" + gameObject.GetInstanceID() + ")");
            return;
        }

        // Needs to be in Awake() because OnHealthChange is called from target's Start()
        target.OnHealthChange += UpdateHealthText;
        healthText = GetComponent<TextMeshProUGUI>();
    }

    private void UpdateHealthText()
    {
        string shields = "";
        if (target.InitialShieldHitpoints > 0)
        {
            shields = "S: " + target.ShieldHitpoints + "/" + target.MaxShieldHitpoints;
        }

        string hull = "";
        if (target.InitialHullHitpoints > 0)
        {
            hull = "H: " + target.HullHitpoints + "/" + target.MaxHullHitpoints;
        }

        string text = (shields + "\n" + hull).Trim();
        healthText.text = text;
    }
}
